from time import localtime

default_resolution = (1296, 972)

def get_camera_for_hour(camera):
    x = localtime()
    if x.tm_hour < 7:
        print('dark camera')
        return dark(camera)
    elif x.tm_hour < 8:
        print('default camera')
        return default_camera(camera)
    elif x.tm_hour < 9:
        print('light camera')
        return light(camera)
    elif x.tm_hour < 15:
        print('full sun camera')
        return full_sun(camera)
    elif x.tm_hour < 16:
        print('light camera')
        return light(camera)
    elif x.tm_hour < 18:
        print('default camera')
        return default_camera(camera)
    else:
        print('dark camera')
        return dark(camera)

def get_interval_for_hour():
    x = localtime()
    if x.tm_hour < 7:
        return 5
    elif x.tm_hour < 9:
        return 1
    elif x.tm_hour < 15:
        return 0.5
    elif x.tm_hour < 18:
        return 1
    else:
        return 5

def dark(camera):
    camera.exposure_mode = 'verylong'
    camera.shutter_speed = 6000000
    camera.iso = 800
    return camera

def default_camera(camera):
    camera.exposure_mode = 'auto'
    camera.shutter_speed = 0
    camera.iso = 200
    return camera

def light(camera):
    camera.exposure_mode = 'auto'
    camera.shutter_speed = 0
    camera.iso = 100
    return camera

def full_sun(camera):
    camera.exposure_mode = 'auto'
    camera.shutter_speed = 1000
    camera.iso = 100
    return camera

def low_res(camera):
    camera.resolution = (640, 480)
    return camera

def default_res(camera):
    camera.resolution = default_resolution
    return camera

def high_res(camera):
    camera.resolution = (2592, 1944)
    return camera

def flip_camera(camera):
    if camera.hflip == True:
        camera.hflip = False
    else:
        camera.hflip = True
    if camera.vflip == True:
        camera.vflip = False
    else:
        camera.vflip = True
    return camera

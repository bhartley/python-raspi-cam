from datetime import datetime
from time import sleep, time
import picamera
import P3picam
import camera_config
from utils import ensure_dir, get_size, human

rootdir = '/home/pi/Desktop/picam'
ensure_dir(rootdir)
cam_name = 'tapo'
window = 3

allowed_size = 2000

def get_file_name_picture(index):
    return rootdir + '/' + cam_name + '-' + str(round(time())) + '-' + str(index) + '.jpg'

def get_file_name_cont():
    return rootdir + '/' + cam_name + '-' + str(round(time())) + '-' + '{counter:04d}.jpg'

continuous = True

if (continuous):
    # continuous capture mode
    with picamera.PiCamera() as camera:
        camera = camera_config.get_camera_for_hour(camera)
        interval = camera_config.get_interval_for_hour()
        itr = 0
        for i, filename in enumerate(camera.capture_continuous(get_file_name_cont())):
            if itr % 1000 == 0:
                camera = camera_config.get_camera_for_hour(camera) # reset camera every so often
            if itr % 10 == 0:
                sz = get_size(rootdir)
                print('currently using ' + human(sz) + ' in rootdir ' + rootdir)
                if sz > (allowed_size * 1024 * 1024):
                    print('using too much space!')
                    #
                    # todo: open up space in rootdir
                    #
            sleep(interval)
            itr += 1
else:
    # motion detection mode
    motionState = False
    while True:
        motionState = P3picam.motion()
        if (motionState):
            with picamera.PiCamera() as camera:
                print('motion, taking pictures at ' + str(datetime.now()))
                camera = camera_config.flip_camera(camera)
                camera = camera_config.default_res(camera)
                camera = camera_config.get_camera_for_hour(camera)
                seconds = 0
                rnd = 0
                while seconds < window:
                    rnd += 1
                    camera.capture(get_file_name_picture(rnd))
                    sleep(interval)
                    seconds += interval
                print('took ' + str(rnd) + ' pictures')
